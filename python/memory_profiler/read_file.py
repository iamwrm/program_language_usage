from typing import Mapping


@profile
def my_func0():

    with open('d1.txt', 'r')as f:
        a = f.readlines()

    len_list = [len(i) for i in a]

    print(len_list[-1])


@profile
def my_func1():

    len_list = []
    with open('d1.txt', 'r')as f:
        for a in f:
            len_list.append(len(a))

    print(len_list[-1])


@profile
def my_func2():

    len_list = []
    with open('d1.txt', 'r')as f:
        pass

    print(0)


my_func0()
my_func1()
my_func2()
